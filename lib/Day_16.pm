package Day_16;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( calculatePhase calculatePhases part2 );

use List::Util qw( sum reduce );

my @basePattern = ( 0, 1, 0, -1 );

use Memoize;

memoize 'pattern';

sub pattern {
    my ($digit, $length) = @_;

    my @output = map { ( $basePattern[ $_ % 4 ] ) x $digit } (0 .. $length);

    return splice @output, 1, $length;
}

sub pattern2 {
    my ($offset, $digit) = @_;

    return $basePattern[ int( $offset / $digit ) % 4 ];
}


sub calculatePhase {
    my @input = split //, $_[0];

    my @output;

    foreach my $digit (1 .. @input) {
        my $i = 1;
        push @output, sum(
            map {
                $_ * pattern2( $i++, $digit );
            } @input
        );
    }

    return join("", map { abs($_) % 10 } @output );
}

sub calculatePhases {
    my ($numberOfPhases, $input) = @_;

    return reduce { calculatePhase( $a ) } $input, 1 .. $numberOfPhases;
}


sub part2 {
    my ( $input ) = @_;
    my $skip = substr( $input, 0, 7 );

    my $digits = substr( ( $input x 10000 ), $skip );

    my $length = length( $digits );

    for ( 1..100 ) {
        my $sum = 0;

        for( my $i = $length - 1; $i >= 0; $i-- ) {
            $sum += substr( $digits, $i, 1 );
            substr( $digits, $i, 1 ) = $sum % 10;
        }
    }

    return substr( $digits,0, 8 );
}

1;
