package Day_05;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( runIntcode );

use constant INDIRECT => 0;
use constant IMMEDIATE => 1;

sub runIntcode {
    my @inputs = @{ shift @_ };
    my @intcode = @_;
    my @outputs;
 
    my %instruction = (
        # Addition
        1 => {
            run => sub {
                my ($pc, $op1, $op2, $op3) = @_;

                $$op3 = $$op1 + $$op2;

                return $pc + 4
            },
            paramLength => 3,
        },

        # Multiplication
        2 => {
            run => sub {
                my ($pc, $op1, $op2, $op3) = @_;

                $$op3 = $$op1 * $$op2;

                return $pc + 4;
            },
            paramLength => 3,
        },

        # Input
        3 => {
            run => sub {
                my ($pc, $op1) = @_;

                my $input = shift @inputs;
                push @inputs, $input;
                $$op1 = $input;

                return $pc + 2;
            },
            paramLength => 1,

        },

        # Output
        4 => {
            run => sub {
                my ($pc, $op1) = @_;

                push @outputs, $$op1;

                return $pc + 2;
            },
            paramLength => 1,
        },

        # Jump-if-true
        5 => {
            run => sub {
                my ($pc, $op1, $op2) = @_;

                return $$op1 ? $$op2 : $pc + 3;
            },
            paramLength => 2,
        },

        # Jump-if-fasle
        6 => {
            run => sub {
                my ($pc, $op1, $op2) = @_;

                return !$$op1 ? $$op2 : $pc + 3;
            },
            paramLength => 2,
        },

        # less than
        7 => {
            run => sub {
                my ($pc, $op1, $op2, $op3) = @_;

                $$op3 = $$op1 < $$op2 ? 1 : 0;

                return $pc + 4;
            },
            paramLength => 3,
        },

        # equals
        8 => {
            run => sub {
                my ($pc, $op1, $op2, $op3) = @_;
                
                $$op3 = $$op1 == $$op2 ? 1 : 0; 

                return $pc + 4;
            },
            paramLength => 3,
        },
    );

    # We start running from location 0
    my $pc = 0;

    # Main VM loop
    while ( $intcode[$pc] != 99 ) {
        my ($opCode, @addressingModes) = parseOpCode( $intcode[$pc] );

        my $op = $instruction{ int( $opCode ) }
            or die "Invalid opcode $opCode";

        my @paramTargets = map {
            operandReference( $_, (shift @addressingModes), \@intcode );

        } ($pc + 1)..($pc + $op->{paramLength} );

        $pc = $op->{run}->( $pc, @paramTargets );
    }

    return {
        grid => \@intcode,
        outputs => \@outputs,
    };
}


sub parseOpCode {
    my ($modes, $opCode) = $_[0] =~ m/^(\d*?)(\d\d?)$/;

    # addressing mode indicators are read right to left, hence the reverse split
    my @addressingModes = reverse split //, $modes;

    return ($opCode, @addressingModes);
}


sub operandReference {
    my ( $operand, $addressingMode, $intcode ) = @_;

    $addressingMode //= 0;

    if ( $addressingMode == INDIRECT ) {
        return \$intcode->[ $intcode->[ $operand ] ];
    }

    if( $addressingMode == IMMEDIATE ) {
        return \$intcode->[ $operand ];
    }

    die "Invalid Addressing Mode `$addressingMode`";
}

1;
