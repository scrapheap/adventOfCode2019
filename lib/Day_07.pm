package Day_07;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( runIntcode runAmplifiers runFeedbackAmplifiers );

use List::Util qw( any );
use Try::Tiny;

use constant IO_WAIT => 'IO Wait';

sub runAmplifiers {
    my @phases = @{ shift @_ };
    my @intcode = @_;

    my $lastOutput = 0;
    foreach my $amplifier ( 0..4 ) {
        my @inputs = ( $phases[ $amplifier ], $lastOutput );
        my $state = runIntcode( undef, \@inputs, @intcode );
        ($lastOutput) = @{$state->{outputs}};
    }

    return $lastOutput;
}

sub runFeedbackAmplifiers {
    my @phases = @{ shift @_ };
    my @intcode = @_;

    my $i = 0;
    my $allFinished = 0;

    my $lastOutputs = [];
    my @processes = map {
        push @{ $lastOutputs }, $_;

        my $state = {
            pc => 0,
            inputs => $lastOutputs,
            outputs => [ ],
            memory => [ @intcode ],
            finished => 0,
        };

        $lastOutputs = $state->{outputs};

        $state;
    } @phases;

    push @{ $processes[0]->{inputs} }, 0;
    $processes[-1]->{outputs} = $processes[0]->{inputs};

    while ( ! $processes[-1]->{finished} ) { 
        foreach my $process ( @processes ) {
            my $state = runIntcode( $process );
        }
    }

    return $processes[-1]->{outputs}->[-1];
}

use constant INDIRECT => 0;
use constant IMMEDIATE => 1;

my %instruction = (
    # Addition
    1 => {
        run => sub {
            my ($state, $op1, $op2, $op3) = @_;

            $$op3 = $$op1 + $$op2;

            $state->{pc} += 4;
            return $state;
        },
        paramLength => 3,
    },

    # Multiplication
    2 => {
        run => sub {
            my ($state, $op1, $op2, $op3) = @_;

            $$op3 = $$op1 * $$op2;

            $state->{pc} += 4;
            return $state;
        },
        paramLength => 3,
    },

    # Input
    3 => {
        run => sub {
            my ($state, $op1) = @_;

            die "IO Wait" unless scalar @{ $state->{inputs} };

            my $input = shift @{ $state->{inputs} };
            $$op1 = $input;

            $state->{pc} += 2;
            return $state;
        },
        paramLength => 1,

    },

    # Output
    4 => {
        run => sub {
            my ($state, $op1) = @_;

            push @{ $state->{outputs} }, $$op1;

            $state->{pc} += 2;
            return $state;
        },
        paramLength => 1,
    },

    # Jump-if-true
    5 => {
        run => sub {
            my ($state, $op1, $op2) = @_;

            $state->{pc} = $$op1 ? $$op2 : $state->{pc} + 3;
            return $state;
        },
        paramLength => 2,
    },

    # Jump-if-fasle
    6 => {
        run => sub {
            my ($state, $op1, $op2) = @_;

            $state->{pc} = !$$op1 ? $$op2 : $state->{pc} + 3;
            return $state;
        },
        paramLength => 2,
    },

    # less than
    7 => {
        run => sub {
            my ($state, $op1, $op2, $op3) = @_;

            $$op3 = $$op1 < $$op2 ? 1 : 0;

            $state->{pc} += 4;
            return $state;
        },
        paramLength => 3,
    },

    # equals
    8 => {
        run => sub {
            my ($state, $op1, $op2, $op3) = @_;
            
            $$op3 = $$op1 == $$op2 ? 1 : 0; 

            $state->{pc} += 4;
            return $state;
        },
        paramLength => 3,
    },

    # halt
    99 => {
        run => sub {
            my ( $state ) = @_;
            $state->{finished} = 1;
            return $state;
        },
        paramLength => 0,
    },
);

sub runIntcode {
    my $state = shift;
    my @inputs = @{ shift // [] };
    my @intcode = @_;
    my @outputs;
 

    # We start running from location 0
    $state //= {
        pc => 0,
        inputs => [ @inputs ],
        outputs => [ ],
        memory => \@intcode,
        finished => 0,
    };

    my $memory = $state->{memory};
    my $running = 1;
    # Main VM loop
    while ( ! $state->{finished} && $running ) {
        my ($opCode, @addressingModes) = parseOpCode( $memory->[$state->{pc}] );

        my $op = $instruction{ int( $opCode ) }
            or die "Invalid opcode $opCode";

        my @paramTargets = map {
            operandReference( $_, (shift @addressingModes), $state->{memory} );

        } ($state->{pc} + 1)..($state->{pc} + $op->{paramLength} );

        try {
            $state = $op->{run}->( $state, @paramTargets );
        } catch {
            if( $_ =~ m/^IO Wait/ ) {
                $running = 0;
            } else {
                warn "$_";
            }
        };
    }

    return $state;
}


sub parseOpCode {
    my ($modes, $opCode) = $_[0] =~ m/^(\d*?)(\d\d?)$/;

    # addressing mode indicators are read right to left, hence the reverse split
    my @addressingModes = reverse split //, $modes;

    return ($opCode, @addressingModes);
}


sub operandReference {
    my ( $operand, $addressingMode, $memory ) = @_;

    $addressingMode //= 0;

    if ( $addressingMode == INDIRECT ) {
        return \$memory->[ $memory->[ $operand ] ];
    }

    if( $addressingMode == IMMEDIATE ) {
        return \$memory->[ $operand ];
    }

    die "Invalid Addressing Mode `$addressingMode`";
}

1;
