package Day_15;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( controlDroid controlDroid2 );

use Try::Tiny;

use List::Util qw( min max reduce first );

use constant IO_WAIT => 'IO Wait';

use constant INDIRECT => 0;
use constant IMMEDIATE => 1;
use constant RELATIVE => 2;

use constant UP    => 1;
use constant DOWN  => 2;
use constant LEFT  => 3;
use constant RIGHT => 4;

use constant WALL   => 0;
use constant FLOOR  => 1;
use constant TARGET => 2;

my %newXY = (
    UP, sub {
        return ($_[0] - 1, $_[1]);
    },
    DOWN, sub {
        return ($_[0] + 1, $_[1]);
    },
    LEFT, sub {
        return ($_[0]    , $_[1] - 1);
    },
    RIGHT, sub {
        return ($_[0]    , $_[1] + 1);
    },
);


sub controlDroid {
    my (@intcode) = @_;

    my %grid=( 0 => { 0 => 1 } );

    my @outputBuffer;

    my %directionCode = (
        8 => UP,
        2 => DOWN,
        4 => LEFT,
        6 => RIGHT,
    );


    my ($x, $y) = (0, 0);
    my $direction;

    my @tile = ( '#', '.', 'O', ' ' );

    my $finished = 0;

    my $state = {
        pc => 0,
        relativeBase => 0,
        inputs => sub {
            $direction = reduce {
                my ($nX, $nY) = $newXY{ $b }->($x, $y);

                defined $grid{$nX}{$nY} ? $a : $b
            } int(rand(4)) + 1, 1 .. 4;

            return $finished ? 0 : $direction;
        },
        outputs => sub {
            my ($result) = @_;

            my ($newX, $newY) = $newXY{ $direction }->($x, $y);

            $grid{$newX}{$newY} = $result;

            if ($result) {
                ($x, $y) = ($newX, $newY);
            }

            if ($result == TARGET) {
                $finished = 1;
            }
        },
        memory => \@intcode,
        finished => 0,
    };

    runIntcode( $state, undef, @intcode );

    my $steps = countSteps( 0, 0, 0, \%grid );

    return ( $steps, %grid );
}


sub controlDroid2 {
    my ($count, @intcode) = @_;

    my %grid=( 0 => { 0 => 1 } );

    my @outputBuffer;

    my %directionCode = (
        8 => UP,
        2 => DOWN,
        4 => LEFT,
        6 => RIGHT,
    );


    my ($x, $y) = (0, 0);
    my $direction;

    my @tile = ( '#', '.', 'O', ' ' );

    my $finished = 0;

    my $state = {
        pc => 0,
        relativeBase => 0,
        inputs => sub {
            $direction = reduce {
                my ($nX, $nY) = $newXY{ $b }->($x, $y);

                defined $grid{$nX}{$nY} ? $a : $b
            } int(rand(4)) + 1, 1 .. 4;

            return $finished ? 0 : $direction;
        },
        outputs => sub {
            my ($result) = @_;

            my ($newX, $newY) = $newXY{ $direction }->($x, $y);

            $grid{$newX}{$newY} = $result;

            if ($result) {
                ($x, $y) = ($newX, $newY);
            }

            if ($count-- < 0) {
                $finished = 1;
            }
        },
        memory => \@intcode,
        finished => 0,
    };

    runIntcode( $state, undef, @intcode );

    my $steps = countSteps( 0, 0, 0, \%grid );

    return ( $steps, %grid );
}


sub countSteps {
    my ($x, $y, $steps, $grid, $seen) = @_;

    no warnings 'recursion';

    $seen //= {};

    return $steps if (( $grid->{$x}{$y} // WALL ) == TARGET);

    return 0 if (( $grid->{$x}{$y} // WALL ) == WALL);
    return 0 if ($seen->{"$x,$y"});

    $seen->{"$x,$y"} = 1;

    $steps = max( map {
        countSteps( $newXY{ $_ }->($x, $y), $steps + 1, $grid, $seen ) || ()
    } (UP, RIGHT, DOWN, LEFT) );

    return $steps // 0;
}


my %instruction = (
    # Addition
    1 => {
        run => sub {
            my ($state, $op1, $op2, $op3) = @_;

            $$op3 = $$op1 + $$op2;

            $state->{pc} += 4;
            return $state;
        },
        paramLength => 3,
    },

    # Multiplication
    2 => {
        run => sub {
            my ($state, $op1, $op2, $op3) = @_;

            $$op3 = $$op1 * $$op2;

            $state->{pc} += 4;
            return $state;
        },
        paramLength => 3,
    },

    # Input
    3 => {
        run => sub {
            my ($state, $op1) = @_;

            if ( ref $state->{inputs} eq "CODE" ) {
                $$op1 = $state->{inputs}->();
            } else {
                die "IO Wait" unless scalar @{ $state->{inputs} };

                $$op1 = shift @{ $state->{inputs} };
            }

            $state->{pc} += 2;
            return $state;
        },
        paramLength => 1,

    },

    # Output
    4 => {
        run => sub {
            my ($state, $op1) = @_;

            if ( ref $state->{outputs} eq "CODE" ) {
                $state->{outputs}->($$op1)
            } else {
               push @{ $state->{outputs} }, $$op1;
            }

            $state->{pc} += 2;
            return $state;
        },
        paramLength => 1,
    },

    # Jump-if-true
    5 => {
        run => sub {
            my ($state, $op1, $op2) = @_;

            $state->{pc} = $$op1 ? $$op2 : $state->{pc} + 3;
            return $state;
        },
        paramLength => 2,
    },

    # Jump-if-fasle
    6 => {
        run => sub {
            my ($state, $op1, $op2) = @_;

            $state->{pc} = !$$op1 ? $$op2 : $state->{pc} + 3;
            return $state;
        },
        paramLength => 2,
    },

    # less than
    7 => {
        run => sub {
            my ($state, $op1, $op2, $op3) = @_;

            $$op3 = $$op1 < $$op2 ? 1 : 0;

            $state->{pc} += 4;
            return $state;
        },
        paramLength => 3,
    },

    # equals
    8 => {
        run => sub {
            my ($state, $op1, $op2, $op3) = @_;
            
            $$op3 = $$op1 == $$op2 ? 1 : 0; 

            $state->{pc} += 4;
            return $state;
        },
        paramLength => 3,
    },

    # adjust the relative base
    9 => {
        run => sub {
            my ( $state, $op1 ) = @_;

            $state->{relativeBase} += $$op1;

            $state->{pc} += 2;
            return $state;
        },
        paramLength => 1,
    },

    # halt
    99 => {
        run => sub {
            my ( $state ) = @_;
            $state->{finished} = 1;
            return $state;
        },
        paramLength => 0,
    },
);

sub runIntcode {
    my $state = shift;
    my @inputs = @{ shift // [] };
    my @intcode = @_;
    my @outputs;
 

    # We start running from location 0
    $state //= {
        pc => 0,
        relativeBase => 0,
        inputs => [ @inputs ],
        outputs => [ ],
        memory => \@intcode,
        finished => 0,
    };

    my $memory = $state->{memory};
    my $running = 1;
    # Main VM loop
    while ( ! $state->{finished} && $running ) {
        my ($opCode, @addressingModes) = parseOpCode( $memory->[$state->{pc}] );

        my $op = $instruction{ int( $opCode ) }
            or die "Invalid opcode $opCode [$state->{pc}]";

        my @paramTargets = map {
            operandReference( $_, (shift @addressingModes), $state );

        } ($state->{pc} + 1)..($state->{pc} + $op->{paramLength} );

        try {
            $state = $op->{run}->( $state, @paramTargets );
        } catch {
            if( $_ =~ m/^IO Wait/ ) {
                $running = 0;
            } else {
                warn "$_";
            }
        };
    }

    return $state;
}


sub parseOpCode {
    my ($modes, $opCode) = $_[0] =~ m/^(\d*?)(\d\d?)$/;

    # addressing mode indicators are read right to left, hence the reverse split
    my @addressingModes = reverse split //, $modes;

    return ($opCode, @addressingModes);
}


sub operandReference {
    my ( $operand, $addressingMode, $state ) = @_;

    $addressingMode //= 0;

    my $memory = $state->{memory};

    if ( $addressingMode == INDIRECT ) {
        $memory->[ $memory->[ $operand ] ] //= 0;
        return \$memory->[ $memory->[ $operand ] ];
    }

    if ( $addressingMode == RELATIVE ) {
        $memory->[ $state->{relativeBase} + $memory->[ $operand ] ] //= 0;
        return \$memory->[ $state->{relativeBase} + $memory->[ $operand ] ];
    }

    if( $addressingMode == IMMEDIATE ) {
        $memory->[ $operand ] //= 0;
        return \$memory->[ $operand ];
    }

    die "Invalid Addressing Mode `$addressingMode`";
}

1;
