package Day_04;

use Modern::Perl;
use Exporter qw( import );
use List::Util qw( reduce );

our @EXPORT_OK = qw( validPasswordAttempt validPasswordAttempt2 );

sub validPasswordAttempt {
    my @password = split //, shift;

    my $doubleDigit;
    
    reduce { $doubleDigit ||= ( $a == $b); $b } @password;

    return 0 unless $doubleDigit;

    my $incrementing = 1;

    reduce { $incrementing &&= ( $a <= $b ); $b } @password;

    return 0 unless $incrementing;

    return 1;
}


sub validPasswordAttempt2 {
    my ($password) = @_;
    my @password = split //, $password;

    $password =~ s/(\d)\1\1+//g;

    return 0 unless $password =~ m/(\d)\1/aa;

    my $incrementing = 1;

    reduce { $incrementing &&= ( $a <= $b ); $b } @password;

    return 0 unless $incrementing;

    return 1;
}


1;
