package Day_01;

use Modern::Perl;

use Exporter qw( import );
our @EXPORT_OK = qw( calculateFuel recursivelyCalculateFuel );

sub calculateFuel {
    my ($mass) = @_;

    return int($mass / 3) - 2;
}

sub recursivelyCalculateFuel {
    my ($mass, $totalFuel ) = @_;

    $totalFuel //= 0;

    my $additionalMass = calculateFuel( $mass );

    return $additionalMass <= 0
        ? $totalFuel // $additionalMass
        : recursivelyCalculateFuel( $additionalMass, $totalFuel + $additionalMass );
}

1;
