package Day_02;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( runIntcode );



sub runIntcode {
    my @intcode = @_;

    my $pc = 0;

    my %runInstruction = (
        1 => sub {
            my ($op1, $op2, $op3) = @_;

            $intcode[$op3] = $intcode[$op1] + $intcode[$op2];
        },
        2 => sub {
            my ($op1, $op2, $op3) = @_;

            $intcode[$op3] = $intcode[$op1] * $intcode[$op2];
        },
    );

    while ( $intcode[$pc] != 99 ) {
        $runInstruction{ $intcode[$pc] }->(@intcode[$pc+1..$pc+3]);
        $pc += 4;
    }

    return @intcode;
}

1;
