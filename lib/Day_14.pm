package Day_14;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( parseReactions initialiseState scaleReactions fuelFromOre );

use POSIX;
use List::Util qw( reduce pairmap);

sub parseReactions {
    my @input = @_;

    my %reactions = map { parseReaction( $_ ) } @input;

    return %reactions;
}


sub parseReaction {
    my ( $line ) = @_;

    my ( $source, $produces ) = $line =~ m/^(.*?)=>(.*?)$/;

    my ( $chem, $qty ) = parseChem( $produces );

    my $reaction = {
        produces => $qty,
        requires => {
            map { parseChem( $_ ) } split /,/, $source
        },
    };

    return ( $chem, $reaction );
}


sub parseChem {
    my ( $ingrediant ) = @_;

    my ( $qty, $chem ) = $ingrediant =~ m/(\d+)\s+(\w+)/;

    return ( $chem, $qty );
}


sub scaleReactions {
    my ( $scale, %reactions ) = @_;

    my %scaledReactions = (
        map {
            $_ => {
                produces => $reactions{ $_ }->{produces} * $scale,
                requires => {
                    pairmap {
                        $a => $b * $scale
                    } %{ $reactions{ $_ }->{requires} }
                },
            },
        } keys %reactions,
    );

    return %scaledReactions;
}


sub initialiseState {
    my %reactions = @_;

    my %state = (
        ORE => {
            qty => 0,
            getUnits => sub {
                my ($required, $state) = @_;

                $state->{ORE}->{qty} -= $required;

                return $state;
            },
        },
        map {
            $_ => {
                qty => 0,
                getUnits => buildStateUpdater( $_ => $reactions{ $_ } )
            } 
        } keys %reactions,
    );

    return \%state;
}


sub buildStateUpdater {
    my ( $element, $reaction ) = @_;

    my %requires = %{ $reaction->{requires} };

    return sub {
        my ($required, $state) = @_;

        if ( $state->{ $element }->{qty} - $required < 0 ) {
            my $need = $required - $state->{ $element }->{qty};

            my $request = int( $need / $reaction->{produces} ) + ($need % $reaction->{produces} ? 1 : 0 );

            $state->{ $element }->{qty} += $reaction->{produces} * $request;

            $state = reduce {
                $a->{ $b }->{getUnits}( $requires{ $b } * $request, $a );
            } $state, keys %requires;

#            while ( $state->{ $element }->{qty} - $required < 0 ) {
#                $state->{ $element }->{qty} += $reaction->{produces};
#            
#                $state = reduce {
#                    $a->{ $b }->{getUnits}( $requires{ $b }, $a );
#                } $state, keys %requires;
#            }
        }

        $state->{ $element }->{qty} -= $required;

        return $state;
    }
}

sub fuelFromOre {
    my ( $reactions, $ore ) = @_;

    my $min = 0;
    my $max = 100000000;

    do {
        my $state = initialiseState( %$reactions );
        $state->{ORE}->{qty} = $ore;

        my $middle = $min + int( ( $max - $min ) / 2);

        $state = $state->{FUEL}->{getUnits}( $middle, $state );

        if ( $state->{ORE}->{qty} >= 0 ) {
            $min = $middle;
        } else {
            $max = $middle;
        }
        
    } while( $max - $min > 2 );

    return $min;
}


sub spareElements {
    my ( $state ) = @_;

    foreach my $elem ( grep { $_ ne "FUEL" && $_ ne "ORE" } keys %$state ) {
        if ( $state->{$elem}->{qty} > 0 ) {
            return 1;
        }
    }

    return 0;
}
1;
