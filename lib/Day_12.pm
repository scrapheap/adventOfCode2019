package Day_12;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( stepForward parseInput totalEnergy stringify stepItRepeatsOn );

use List::Util qw( sum );
use Math::Utils qw( lcm );

sub stepForward {
    my @currentMoons = @_;

    foreach my $currentMoon (@currentMoons) {
        MOON:
        foreach my $otherMoon (@currentMoons) {
            next MOON if $currentMoon == $otherMoon;

            $currentMoon->{vX} += gravity( $currentMoon->{x}, $otherMoon->{x} );
            $currentMoon->{vY} += gravity( $currentMoon->{y}, $otherMoon->{y} );
            $currentMoon->{vZ} += gravity( $currentMoon->{z}, $otherMoon->{z} );
        }
    }

    my @nextMoons = map {
        $_->{x} += $_->{vX};
        $_->{y} += $_->{vY};
        $_->{z} += $_->{vZ};

        $_
    } @currentMoons;

    return @nextMoons;
}


sub gravity {
    my ($a, $b) = @_;

    return $b <=> $a;
}


sub parseInput {
    my @input = map {
        my $coords = { $_ =~ m/<\s*(x)\s*=\s*(-?\d+)\s*,\s*(y)\s*=\s*(-?\d+)\s*,\s*(z)\s*=\s*(-?\d+)\s*>/ };

        $coords->{x} ? $coords : ();
    } @_;

    return @input;
}

sub totalEnergy {
    my @moons = @_;

    my $totalEnergy = sum(
        map { sum( abs $_->{x}, abs $_->{y}, abs $_->{z} ) * sum( abs $_->{vX}, abs $_->{vY}, abs $_->{vZ} ) } @moons
    );

    return $totalEnergy;
}

sub stringify {
    my $axis = shift;

    my $string = join(",", map { map { $_ // 0 } @{$_}{ $axis, "v". uc($axis)} } @_ );

    return $string
}

sub stepItRepeatsOn {
    my @moons = @_;

    my %seenBefore;
    my %repeatsOn;

    my $step = 0;
    while (!$repeatsOn{x} || !$repeatsOn{y} || !$repeatsOn{z})  {
        foreach my $axis ( qw( x y z ) ) {
            my $axisId = stringify( $axis, @moons );
            if ( $seenBefore{ $axis }->{ $axisId } ) {
                $repeatsOn{ $axis } //= $step;
            }
            $seenBefore{ $axis }->{ $axisId } = 1;
        }
        @moons = stepForward( @moons );
        $step++;
    }

    return lcm( @repeatsOn{ qw( x y z ) } );
}

1;
