package Day_17;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( parseImage alignGrid runIntcode );

use List::Util qw( all );

use constant SCAFFOLD => 35;

sub parseImage {
    my @image = @_;
    
    my @grid = [];

    while( my $value = shift @image ) {
        if ( $value == 10 ) {
            push @grid, [];
        } else {
            push @{ $grid[-1] }, $value;
        }
    }

    pop @grid unless scalar @{ $grid[-1] };

    return @grid;
}

sub alignGrid {
    my @grid = @_;

    my $alignment = 0;

    foreach my $x ( 1 .. @grid - 3 ) {
        foreach my $y ( 1 .. @{ $grid[$x] } - 2 ) {
            #warn "$x, $y\n";
            if ( all { $_ == SCAFFOLD } $grid[$x][$y], $grid[$x + 1][$y], $grid[$x - 2][$y], $grid[$x ][$y - 1], $grid[$x][$y + 1] ) {
                $alignment += $x * $y;
            }
        }
    }

    return $alignment;
}

use Try::Tiny;

use constant IO_WAIT => 'IO Wait';

use constant INDIRECT => 0;
use constant IMMEDIATE => 1;
use constant RELATIVE => 2;


my %instruction = (
    # Addition
    1 => {
        run => sub {
            my ($state, $op1, $op2, $op3) = @_;

            $$op3 = $$op1 + $$op2;

            $state->{pc} += 4;
            return $state;
        },
        paramLength => 3,
    },

    # Multiplication
    2 => {
        run => sub {
            my ($state, $op1, $op2, $op3) = @_;

            $$op3 = $$op1 * $$op2;

            $state->{pc} += 4;
            return $state;
        },
        paramLength => 3,
    },

    # Input
    3 => {
        run => sub {
            my ($state, $op1) = @_;

            if ( ref $state->{inputs} eq "CODE" ) {
                $$op1 = $state->{inputs}->();
            } else {
                die "IO Wait" unless scalar @{ $state->{inputs} };

                $$op1 = shift @{ $state->{inputs} };
            }

            $state->{pc} += 2;
            return $state;
        },
        paramLength => 1,

    },

    # Output
    4 => {
        run => sub {
            my ($state, $op1) = @_;

            if ( ref $state->{outputs} eq "CODE" ) {
                $state->{outputs}->($$op1)
            } else {
               push @{ $state->{outputs} }, $$op1;
            }

            $state->{pc} += 2;
            return $state;
        },
        paramLength => 1,
    },

    # Jump-if-true
    5 => {
        run => sub {
            my ($state, $op1, $op2) = @_;

            $state->{pc} = $$op1 ? $$op2 : $state->{pc} + 3;
            return $state;
        },
        paramLength => 2,
    },

    # Jump-if-fasle
    6 => {
        run => sub {
            my ($state, $op1, $op2) = @_;

            $state->{pc} = !$$op1 ? $$op2 : $state->{pc} + 3;
            return $state;
        },
        paramLength => 2,
    },

    # less than
    7 => {
        run => sub {
            my ($state, $op1, $op2, $op3) = @_;

            $$op3 = $$op1 < $$op2 ? 1 : 0;

            $state->{pc} += 4;
            return $state;
        },
        paramLength => 3,
    },

    # equals
    8 => {
        run => sub {
            my ($state, $op1, $op2, $op3) = @_;
            
            $$op3 = $$op1 == $$op2 ? 1 : 0; 

            $state->{pc} += 4;
            return $state;
        },
        paramLength => 3,
    },

    # adjust the relative base
    9 => {
        run => sub {
            my ( $state, $op1 ) = @_;

            $state->{relativeBase} += $$op1;

            $state->{pc} += 2;
            return $state;
        },
        paramLength => 1,
    },

    # halt
    99 => {
        run => sub {
            my ( $state ) = @_;
            $state->{finished} = 1;
            return $state;
        },
        paramLength => 0,
    },
);

sub runIntcode {
    my $state = shift;
    my @inputs = @{ shift // [] };
    my @intcode = @_;
    my @outputs;
 

    # We start running from location 0
    $state //= {
        pc => 0,
        relativeBase => 0,
        inputs => [ @inputs ],
        outputs => [ ],
        memory => \@intcode,
        finished => 0,
    };

    my $memory = $state->{memory};
    my $running = 1;
    # Main VM loop
    while ( ! $state->{finished} && $running ) {
        my ($opCode, @addressingModes) = parseOpCode( $memory->[$state->{pc}] );

        my $op = $instruction{ int( $opCode ) }
            or die "Invalid opcode $opCode [$state->{pc}]";

        my @paramTargets = map {
            operandReference( $_, (shift @addressingModes), $state );

        } ($state->{pc} + 1)..($state->{pc} + $op->{paramLength} );

        try {
            $state = $op->{run}->( $state, @paramTargets );
        } catch {
            if( $_ =~ m/^IO Wait/ ) {
                $running = 0;
            } else {
                warn "$_";
            }
        };
    }

    return $state;
}


sub parseOpCode {
    my ($modes, $opCode) = $_[0] =~ m/^(\d*?)(\d\d?)$/;

    # addressing mode indicators are read right to left, hence the reverse split
    my @addressingModes = reverse split //, $modes;

    return ($opCode, @addressingModes);
}


sub operandReference {
    my ( $operand, $addressingMode, $state ) = @_;

    $addressingMode //= 0;

    my $memory = $state->{memory};

    if ( $addressingMode == INDIRECT ) {
        $memory->[ $memory->[ $operand ] ] //= 0;
        return \$memory->[ $memory->[ $operand ] ];
    }

    if ( $addressingMode == RELATIVE ) {
        $memory->[ $state->{relativeBase} + $memory->[ $operand ] ] //= 0;
        return \$memory->[ $state->{relativeBase} + $memory->[ $operand ] ];
    }

    if( $addressingMode == IMMEDIATE ) {
        $memory->[ $operand ] //= 0;
        return \$memory->[ $operand ];
    }

    die "Invalid Addressing Mode `$addressingMode`";
}

1;
