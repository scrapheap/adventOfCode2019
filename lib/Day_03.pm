package Day_03;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( findNearestCrossing findNearestCrossingStepwise );

use List::Util qw( min sum );

sub findNearestCrossing {
    my @wires = @_;

    my $grid = {};

    my $wireNo = 1;
    foreach my $wire (@wires) {
        runWire( $grid, $wireNo++, @$wire );
    }

    my @crossings;

    foreach my $x (keys %$grid) {
        foreach my $y (keys %{ $grid->{$x} }) {
            if (scalar @wires == scalar keys %{ $grid->{$x}{$y} }) {
                push @crossings, abs($x) + abs($y);
            }
        }

    }

    return min( @crossings );
}

sub runWire {
    my ($grid, $wireNo, @wire ) = @_;

    my %updateLoc = (
        U => sub { ( $_[0],     $_[1] + 1 ) },
        D => sub { ( $_[0],     $_[1] - 1 ) },
        L => sub { ( $_[0] - 1, $_[1]     ) },
        R => sub { ( $_[0] + 1, $_[1]     ) },
    );

    my $stepCount = 1;
    my ( $x, $y ) = ( 0, 0 );

    foreach my $step (@wire) {
        my ($direction, $distance) = $step =~ m/(\w)(\d+)/;

        while($distance--) {
            ($x, $y) = $updateLoc{ uc( $direction ) }->( $x, $y );
            $grid->{ $x }{ $y }{ $wireNo } = $stepCount++; 
        }
    }
}

sub findNearestCrossingStepwise {
    my @wires = @_;

    my $grid = {};

    my $wireNo = 1;
    foreach my $wire (@wires) {
        runWire( $grid, $wireNo++, @$wire );
    }

    my @crossings;

    foreach my $x (keys %$grid) {
        foreach my $y (keys %{ $grid->{$x} }) {
            if (scalar @wires == scalar keys %{ $grid->{$x}{$y} }) {
                push @crossings, sum( values %{ $grid->{$x}{$y} } );
            }
        }
    }

    return min( @crossings );
}

1;
