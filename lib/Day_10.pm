package Day_10;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( parseAsteroidMap countAngles destructionOrder );

use Math::Trig;


sub parseAsteroidMap {
    my @rows = @_;

    my $y = 0;

    my @asteroidMap;

    foreach my $row (@rows) {
        my $x = 0;
        foreach my $cell (split //, $row) {
            if ( $cell eq "#" ) {
                push @asteroidMap, [$x, $y];
            }
            $x++;
        }

        $y++;
    }

    return @asteroidMap;
}

sub countAngles {
    my @asteroidMap = @_;

    my %angles;

    foreach my $targetAsteroid (@asteroidMap) {
        ASTEROID:
        foreach my $asteroid (@asteroidMap) {
            next ASTEROID if $targetAsteroid == $asteroid;
            my $deltaX = $asteroid->[0] - $targetAsteroid->[0];
            my $deltaY = $asteroid->[1] - $targetAsteroid->[1];
            my $angleBetween = atan2( $deltaX, $deltaY );
            $angles{ join(",", @$targetAsteroid) }->{$angleBetween} = 1;
        }
    }

    return map { $_ => scalar keys %{ $angles{ $_ } } } keys %angles;
}

sub destructionOrder {
    my ($targetAsteroid, @asteroidMap) = @_;

    my %angles;

    ASTEROID:
    foreach my $asteroid (@asteroidMap) {
        next ASTEROID if ($targetAsteroid->[0] == $asteroid->[0] && $targetAsteroid->[1] == $asteroid->[1]);
        my $deltaX = $asteroid->[0] - $targetAsteroid->[0];
        my $deltaY = $asteroid->[1] - $targetAsteroid->[1];
        my $angleBetween = atan2( $deltaX, $deltaY );
        $angles{$angleBetween}->{ distanceBetween($targetAsteroid, $asteroid) } = $asteroid;
    }

    my %degAngles;
    foreach my $angle ( keys %angles ) {
        my $degAngle = 180 + rad2deg( $angle );

        $degAngles{ $degAngle } = [ map { $angles{ $angle }->{ $_ } } sort keys %{ $angles{ $angle } } ];
    }

    my @destructionOrder;

    while( keys %degAngles ) {
        foreach my $angle ( sort {$b <=> $a} keys %degAngles ) {
            push @destructionOrder, shift @{ $degAngles{$angle} };

            delete $degAngles{$angle} unless @{ $degAngles{$angle} };
        }
    }

    return @destructionOrder; 
}

sub distanceBetween {
    my ($a, $b) = @_;

    my $horizontal = abs($a->[0] - $b->[0]);
    my $vertical = abs($a->[1] - $b->[1]);

    return sqrt( ($horizontal ** 2) + ($vertical ** 2) );
}

1;
