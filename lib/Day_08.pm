package Day_08;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( splitIntoLayers );

sub splitIntoLayers {
    my ( $width, $height, $data ) = @_;

    my @imageData = split //, $data;

    my @layers;

    while ( @imageData ) {
        push @layers, [ splice @imageData, 0, $width * $height ];
    }

    return @layers;
}

1;

