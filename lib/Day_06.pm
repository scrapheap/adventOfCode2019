package Day_06;

use Modern::Perl;
use Exporter qw( import );
use List::Util qw( reduce );

our @EXPORT_OK = qw( orbitCountChecksum hopsBetween );

sub orbitCountChecksum {
    my @orbitMap = grep { $_ =~ m/\)/ }  @_;

    my $orbits = reduce {
        my ($orbitee, $orbitor) = $b =~ m/(\w+)\s*\)\s*(\w+)/;

        push @{ $a->{$orbitee} }, $orbitor;

        $a
    } {}, @orbitMap;

    calculateChecksum( 'COM', $orbits, 0 );
}

sub calculateChecksum {
    my ( $orbitee, $orbits, $count ) = @_;

    my $checksum = $count;
    foreach my $orbitor (@{ $orbits->{ $orbitee } // [] }) {
        $checksum += calculateChecksum( $orbitor, $orbits, $count + 1 );
    }

    return $checksum;
}


sub hopsBetween {
    my $from = shift;
    my $to = shift;
    my @orbitMap = grep { $_ =~ m/\)/ } @_;

    my $orbits = reduce {
        my ($orbitee, $orbitor) = $b =~ m/(\w+)\s*\)\s*(\w+)/;

        push @{ $a->{$orbitee} }, $orbitor;
        push @{ $a->{$orbitor} }, $orbitee;

        $a
    } {}, @orbitMap;

    my $fromOrbitting = $orbits->{ $from }[0];

    return calculateHopsBetween( $fromOrbitting, $to, $orbits, {} );
}


sub calculateHopsBetween {
    my ( $from, $to, $orbits, $seen, @hops ) = @_;

    $seen->{ $from } = 1;

    POSSIBLE_HOP:
    foreach my $hop ( @{ $orbits->{$from} // [] } ) {
        if( $hop eq $to ) {
            return @hops;
        }

        next POSSIBLE_HOP if $seen->{$hop};

        my @chain = calculateHopsBetween( $hop, $to, $orbits, $seen, @hops, $hop );

        if ( @chain ) {
           return @chain;
        }
    }

    # we didn't find our target in our children so return nothing
    return;
}

1;
