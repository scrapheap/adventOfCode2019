use Modern::Perl;

use Day_08 qw( splitIntoLayers );

use Test::More;
use Test::Deep;

cmp_deeply(
    [ splitIntoLayers( 3, 2, "123456789012" ) ],
    [
        [
            1,2,3, 4,5,6,
        ], [
            7,8,9, 0,1,2,
        ],
    ],
    "splitIntoLayers should split the image into a set of layers"
);

done_testing();
