use Modern::Perl;

use Day_06 qw( orbitCountChecksum hopsBetween );

use Test::More;
use Test::Deep;

my @orbitMap = (
    'COM)B',
    'B)C',
    'C)D',
    'D)E',
    'E)F',
    'B)G',
    'G)H',
    'D)I',
    'E)J',
    'J)K',
    'K)L',
);

is( orbitCountChecksum( @orbitMap ),  42,  "The orbit count checksum of our map should be 42" );

my @orbitMap2 = (
    'COM)B',
    'B)C',
    'C)D',
    'D)E',
    'E)F',
    'B)G',
    'G)H',
    'D)I',
    'E)J',
    'J)K',
    'K)L',
    'K)YOU',
    'I)SAN',
);

cmp_deeply(
    [ hopsBetween( 'YOU', 'SAN', @orbitMap2 ) ],
    [ qw( J E D I )],
    "The hops between `YOU` and `SAN` should be 4"
);

done_testing();
