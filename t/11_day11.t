use Modern::Perl;

use Day_11 qw( runPaintingBot );

use Test::More;
use Test::Deep;

use constant LEFT => 0;
use constant RIGHT => 1;

use constant FACING_UP => 0;
use constant FACING_RIGHT => 1;
use constant FACING_DOWN => 2;
use constant FACING_LEFT => 3;

my @testArray = ( FACING_UP, FACING_RIGHT, FACING_DOWN, FACING_LEFT );

@testArray = $Day_11::turn[ LEFT ]->(@testArray);

cmp_deeply(
    \@testArray,
    [ FACING_LEFT, FACING_UP, FACING_RIGHT, FACING_DOWN ],
    "Turning LEFT should move us to facing left"
);

@testArray = $Day_11::turn[ RIGHT ]->(@testArray);
cmp_deeply(
    \@testArray,
    [ FACING_UP, FACING_RIGHT, FACING_DOWN, FACING_LEFT ],
    "Turning RIGHT should move us to facing up again"
);

@testArray = $Day_11::turn[ RIGHT ]->(@testArray);
cmp_deeply(
    \@testArray,
    [ FACING_RIGHT, FACING_DOWN, FACING_LEFT, FACING_UP ],
    "Turning RIGHT should move us to facing right"
);


my @outputs = (
    [1, 0],
    [0, 0],
    [1, 0],
    [1, 0],
    [0, 1],
    [1, 0],
    [1, 0],
);

{
    no warnings 'redefine';
    
    sub Day_11::runIntcode {
        my ($state) = @_;

        foreach my $output ( map { @{ $_ } } @outputs ) {
            $state->{outputs}->($output);
        }
    }
}

my %grid = runPaintingBot( 0 );

cmp_deeply(
    \%grid,
    {
        -1 => {
            1 => 1,
        },
        0 => {
            -1 => 0,
            0 => 0,
            1 => 1,
        },
        1 => {
            -1 => 1,
            0 => 1,
        },
    },
    "When fed our outputs the painting bot should produce the pattern we expect"
);

is( scalar ( map { keys %{ $_ } } values %grid ), 6, "in total 6 cells should have been painted" );

@outputs = (
    [1, 0],
    [0, 0],
    [1, 0],
    [1, 0],
    [0, 1],
    [1, 0],
    [1, 0],
);

%grid = runPaintingBot( 1 );

cmp_deeply(
    \%grid,
    {
        -1 => {
            1 => 1,
        },
        0 => {
            -1 => 0,
            0 => 0,
            1 => 1,
        },
        1 => {
            -1 => 1,
            0 => 1,
        },
    },
    "When fed our outputs the painting bot should produce the pattern we expect"
);

done_testing();
