use Modern::Perl;

use Day_17 qw( parseImage alignGrid );

use Test::More;
use Test::Deep;

my @image = (
46,46,35,46,46,46,46,46,46,46,46,46,46,10,
46,46,35,46,46,46,46,46,46,46,46,46,46,10,
35,35,35,35,35,35,35,46,46,46,35,35,35,10,
35,46,35,46,46,46,35,46,46,46,35,46,35,10,
35,35,35,35,35,35,35,35,35,35,35,35,35,10,
46,46,35,46,46,46,35,46,46,46,35,46,46,10,
46,46,35,35,35,35,35,46,46,46,94,46,46,10,
);

my @grid = parseImage( @image );

cmp_deeply(
    \@grid,
    [        
        [ 46,46,35,46,46,46,46,46,46,46,46,46,46 ],
        [ 46,46,35,46,46,46,46,46,46,46,46,46,46 ],
        [ 35,35,35,35,35,35,35,46,46,46,35,35,35 ],
        [ 35,46,35,46,46,46,35,46,46,46,35,46,35 ],
        [ 35,35,35,35,35,35,35,35,35,35,35,35,35 ],
        [ 46,46,35,46,46,46,35,46,46,46,35,46,46 ],
        [ 46,46,35,35,35,35,35,46,46,46,94,46,46 ],
    ],
    "The parsed grid should make sense"
);

is( alignGrid( @grid ), 76 , "Our grid has an alignment value of 76" ); 

done_testing();
