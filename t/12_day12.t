use Modern::Perl;

use Day_12 qw( stepForward parseInput totalEnergy stringify stepItRepeatsOn );

use Test::More;
use Test::Deep;

cmp_deeply(
    [ parseInput(
        '<x=-1, y=0, z=2>',
        '<x=2, y=-10, z=-7>',
        '<x=4, y=-8, z=8>',
        '<x=3, y=5, z=-1>',
    ) ],
    [
        { x => -1, y =>   0, z =>  2 },
        { x =>  2, y => -10, z => -7 },
        { x =>  4, y =>  -8, z => 8 },
        { x =>  3, y =>   5, z => -1 },
    ],
    "Our parsed input should produce the data structure we expect"
);


my @moons = stepForward(
    { id => 1, x => -1, y => 0,   z => 2 },
    { id => 2, x => 2,  y => -10, z => -7 },
    { id => 3, x => 4,  y => -8,  z => 8 },
    { id => 4, x => 3,  y => 5,   z => -1 },
);

cmp_deeply(
    \@moons,
    [
        { id => 1, x => 2, y => -1, z =>  1, vX =>  3, vY => -1, vZ => -1},
        { id => 2, x => 3, y => -7, z => -4, vX =>  1, vY =>  3, vZ =>  3},
        { id => 3, x => 1, y => -7, z =>  5, vX => -3, vY =>  1, vZ => -3},
        { id => 4, x => 2, y =>  2, z =>  0, vX => -1, vY => -3, vZ =>  1},
    ],
    "One step forward should result in our moons being in the expected location and moving at the expected velocity"
);

@moons = stepForward( @moons );

cmp_deeply(
    \@moons,
    [
        { id => 1, x => 5, y => -3, z => -1, vX =>  3, vY => -2, vZ => -2},
        { id => 2, x => 1, y => -2, z =>  2, vX => -2, vY =>  5, vZ =>  6},
        { id => 3, x => 1, y => -4, z => -1, vX =>  0, vY =>  3, vZ => -6},
        { id => 4, x => 1, y => -4, z =>  2, vX => -1, vY => -6, vZ =>  2},
    ],
    "Two steps forward should result in our moons being in the expected location and moving at the expected velocity"
);

for (3..10) {
    @moons = stepForward( @moons );
}

cmp_deeply(
    \@moons,
    [
        { id => 1, x => 2, y =>  1, z => -3, vX => -3, vY => -2, vZ =>  1},
        { id => 2, x => 1, y => -8, z =>  0, vX => -1, vY =>  1, vZ =>  3},
        { id => 3, x => 3, y => -6, z =>  1, vX =>  3, vY =>  2, vZ => -3},
        { id => 4, x => 2, y =>  0, z =>  4, vX =>  1, vY => -1, vZ => -1},
    ],
    "After 10 steps we should have the expected location and velocity for each moon"
);

is( totalEnergy( @moons ), 179, "The total energy of the moons after 10 steps should be 179");

is( stringify( 'x', @moons ), "2,-3,1,-1,3,3,2,1", "stringified output should be just the values for the x axis comma separated");
is( stringify( 'y', @moons ), "1,-2,-8,1,-6,2,0,-1", "stringified output should be just the values for the y axis comma separated");
is( stringify( 'z', @moons ), "-3,1,0,3,1,-3,4,-1", "stringified output should be just the values for the z axis comma separated");

is( stringify( 'x', { x => 2, y =>  1, z => -3 } ), "2,0", "Stringify should make sure that it defaults missing values to 0");
is( stringify( 'y', { x => 2, y =>  1, z => -3 } ), "1,0", "Stringify should make sure that it defaults missing values to 0");
is( stringify( 'z', { x => 2, y =>  1, z => -3 } ), "-3,0", "Stringify should make sure that it defaults missing values to 0");


@moons = ( 
    { x => -1, y =>   0, z =>  2 },
    { x =>  2, y => -10, z => -7 },
    { x =>  4, y =>  -8, z => 8 },
    { x =>  3, y =>   5, z => -1 },
);

is( stepItRepeatsOn( @moons ), 2772, "It should repeat itself after 2772 steps" );

@moons = parseInput(
    '<x=-8, y=-10, z=0>',
    '<x=5, y=5, z=10>',
    '<x=2, y=-7, z=3>',
    '<x=9, y=-8, z=-3>',
);

is( stepItRepeatsOn( @moons ), 4686774924, "It should repeat itself after 4686774924 steps" );

done_testing();
