use Modern::Perl;

use Day_05 qw( runIntcode );

use Test::More;
use Test::Deep;

cmp_deeply(
    runIntcode( [], 1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50 )->{grid},
    [ 3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50 ],
    "First test program should finish in the expected state"
);

cmp_deeply(
    runIntcode( [], 1,0,0,0,99 )->{grid},
    [ 2,0,0,0,99 ],
    "Second test program should finish in the expected state"
);

cmp_deeply(
    runIntcode( [], 2,3,0,3,99 )->{grid},
    [ 2,3,0,6,99 ],
    "Third test program should finish in the expected state"
);

cmp_deeply(
    runIntcode( [], 2,4,4,5,99,0 )->{grid},
    [ 2,4,4,5,99,9801 ],
    "Fourth test program should finish in the expected state"
);

cmp_deeply(
    runIntcode( [], 1,1,1,4,99,5,6,0,99 )->{grid},
    [ 30,1,1,4,2,5,6,0,99 ],
    "Fifth test program should finish in the expected state"
);

cmp_deeply(
    runIntcode( [ 1 ], 3,0,4,0,99 )->{outputs},
    [ 1 ],
    "Our output for this progam should be the same as the input"
);

cmp_deeply(
    runIntcode( [], 1002,4,3,4,33 )->{grid},
    [ 1002, 4, 3, 4, 99 ],
    "Our grid for this program should be in the expected state"
);

cmp_deeply(
    runIntcode( [ 8 ], 3,9,8,9,10,9,4,9,99,-1,8 )->{outputs},
    [ 1 ],
    "Checking if 8 is 8 should return 1"
);

cmp_deeply(
    runIntcode( [ 1 ], 3,9,8,9,10,9,4,9,99,-1,8 )->{outputs},
    [ 0 ],
    "Checking if 1 is 8 should return 0"
);

cmp_deeply(
    runIntcode( [ 1 ], 3,9,7,9,10,9,4,9,99,-1,8 )->{outputs},
    [ 1 ],
    "Checking if 1 is less than 8 should return 1"
);

cmp_deeply(
    runIntcode( [ 8 ], 3,9,7,9,10,9,4,9,99,-1,8 )->{outputs},
    [ 0 ],
    "Checking if 8 is less than 8 should return 0"
);

cmp_deeply(
    runIntcode( [ 8 ], 3,3,1108,-1,8,3,4,3,99 )->{outputs},
    [ 1 ],
    "Checking if input of 8 is equal to 8 should return 1"
);

cmp_deeply(
    runIntcode( [ 7 ], 3,3,1108,-1,8,3,4,3,99 )->{outputs},
    [ 0 ],
    "Checking if input of 7 is equal to 8 should return 0"
);

cmp_deeply(
    runIntcode( [ 1 ], 3,3,1107,-1,8,3,4,3,99 )->{outputs},
    [ 1 ],
    "Checking if 1 is less than 8 should return 1"
);

cmp_deeply(
    runIntcode( [ 8 ], 3,3,1107,-1,8,3,4,3,99 )->{outputs},
    [ 0 ],
    "Checking if 8 is less than 8 should return 0"
);

cmp_deeply(
    runIntcode( [ 0 ], 3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9 )->{outputs},
    [ 0 ],
    "This one should return 0 as we have passed 0"
);

cmp_deeply(
    runIntcode( [ 10 ], 3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9 )->{outputs},
    [ 1 ],
    "This one should return 1 as we have passed a number larger than 0"
);

cmp_deeply(
    runIntcode( [ 0 ], 3,3,1105,-1,9,1101,0,0,12,4,12,99,1 )->{outputs},
    [ 0 ],
    "This one should return 0 as we have passed 0"
);

cmp_deeply(
    runIntcode( [ 10 ], 3,3,1105,-1,9,1101,0,0,12,4,12,99,1 )->{outputs},
    [ 1 ],
    "This one should return 1 as we have passed a number larger than 0"
);


cmp_deeply(
    runIntcode( [ 7 ], 3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
        1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
        999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99)->{outputs},
    [ 999 ],
    "This one should return 999 as the input is below 8"
);

cmp_deeply(
    runIntcode( [ 8 ], 3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
        1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
        999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99)->{outputs},
    [ 1000 ],
    "This one should return 1000 as the input is 8"
);

cmp_deeply(
    runIntcode( [ 9 ], 3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
        1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
        999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99)->{outputs},
    [ 1001 ],
    "This one should return 1001 as the input is above 8"
);

done_testing();
