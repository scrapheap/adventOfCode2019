use Modern::Perl;

use Day_01 qw( calculateFuel recursivelyCalculateFuel );

use Test::More;

is( calculateFuel(12), 2, "Mass of 12 should require 2 units of fuel" );
is( calculateFuel(14), 2, "Mass of 14 should require 2 units of fuel" );
is( calculateFuel(1969), 654, "Mass of 1969 should require 654 units of fuel" );
is( calculateFuel(100756), 33583, "Mass of 100756 should require 33583 units of fuel" );

is( recursivelyCalculateFuel(14), 2, "Mass of 14 should require 2 units of fuel");
is( recursivelyCalculateFuel(1969), 966, "Mass of 1969 should require 966 units of fuel");
is( recursivelyCalculateFuel(100756),  50346, "Mass of 100756 should require 50346 units of fuel");

done_testing();
