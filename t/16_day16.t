use Modern::Perl;

use Day_16 qw( calculatePhase calculatePhases part2 );

use Test::More;
use Test::Deep;

cmp_deeply( [ Day_16::pattern(1, 8) ], [ 1, 0, -1, 0, 1, 0, -1, 0], "The pattern produced for digit one should be the base pattern simply repeated to fill the length and shifted left once" );
cmp_deeply( [ Day_16::pattern(2, 8) ], [ 0, 1, 1, 0, 0, -1, -1, 0], "The pattern produced for digit two should be the a mapping of the base pattern shifted left twice" );
cmp_deeply( [ Day_16::pattern(3, 8) ], [ 0, 0, 1, 1, 1, 0, 0, 0], "The pattern produced for digit three should be the a mapping of the base pattern shifted left thrice" );

is( Day_16::pattern2(1, 1), 1,  "The pattern produced for digit one should be the base pattern simply repeated to fill the length and shifted left once" );
is( Day_16::pattern2(2, 1), 0,  "The pattern produced for digit one should be the base pattern simply repeated to fill the length and shifted left once" );
is( Day_16::pattern2(3, 1), -1, "The pattern produced for digit one should be the base pattern simply repeated to fill the length and shifted left once" );
is( Day_16::pattern2(4, 1), 0,  "The pattern produced for digit one should be the base pattern simply repeated to fill the length and shifted left once" );
is( Day_16::pattern2(5, 1), 1,  "The pattern produced for digit one should be the base pattern simply repeated to fill the length and shifted left once" );
is( Day_16::pattern2(6, 1), 0,  "The pattern produced for digit one should be the base pattern simply repeated to fill the length and shifted left once" );
is( Day_16::pattern2(7, 1), -1, "The pattern produced for digit one should be the base pattern simply repeated to fill the length and shifted left once" );
is( Day_16::pattern2(8, 1), 0,  "The pattern produced for digit one should be the base pattern simply repeated to fill the length and shifted left once" );

is( Day_16::pattern2(1, 2), 0,  "The pattern produced for digit two should be the a mapping of the base pattern shifted left twice" );
is( Day_16::pattern2(2, 2), 1,  "The pattern produced for digit two should be the a mapping of the base pattern shifted left twice" );
is( Day_16::pattern2(3, 2), 1,  "The pattern produced for digit two should be the a mapping of the base pattern shifted left twice" );
is( Day_16::pattern2(4, 2), 0,  "The pattern produced for digit two should be the a mapping of the base pattern shifted left twice" );
is( Day_16::pattern2(5, 2), 0,  "The pattern produced for digit two should be the a mapping of the base pattern shifted left twice" );
is( Day_16::pattern2(6, 2), -1, "The pattern produced for digit two should be the a mapping of the base pattern shifted left twice" );
is( Day_16::pattern2(7, 2), -1, "The pattern produced for digit two should be the a mapping of the base pattern shifted left twice" );
is( Day_16::pattern2(8, 2), 0,  "The pattern produced for digit two should be the a mapping of the base pattern shifted left twice" );

is( Day_16::pattern2(1, 3), 0, "The pattern produced for digit three should be the a mapping of the base pattern shifted left thrice" );
is( Day_16::pattern2(2, 3), 0, "The pattern produced for digit three should be the a mapping of the base pattern shifted left thrice" );
is( Day_16::pattern2(3, 3), 1, "The pattern produced for digit three should be the a mapping of the base pattern shifted left thrice" );
is( Day_16::pattern2(4, 3), 1, "The pattern produced for digit three should be the a mapping of the base pattern shifted left thrice" );
is( Day_16::pattern2(5, 3), 1, "The pattern produced for digit three should be the a mapping of the base pattern shifted left thrice" );
is( Day_16::pattern2(6, 3), 0, "The pattern produced for digit three should be the a mapping of the base pattern shifted left thrice" );
is( Day_16::pattern2(7, 3), 0, "The pattern produced for digit three should be the a mapping of the base pattern shifted left thrice" );
is( Day_16::pattern2(8, 3), 0, "The pattern produced for digit three should be the a mapping of the base pattern shifted left thrice" );

is( calculatePhase( '12345678' ), '48226158', "After the first phase we should have the value we expect" );
is( calculatePhase( '48226158' ), '34040438', "After the second phase we should have the value we expect" );
is( calculatePhase( '34040438' ), '03415518', "After the third phase we should have the value we expect" );
is( calculatePhase( '03415518' ), '01029498', "After the fourth phase we should have the value we expect" );

is( substr( calculatePhases( 100, '80871224585914546619083218645595' ), 0, 8), '24176176', "100 phases should produce the value we expect" );
is( substr( calculatePhases( 100, '19617804207202209144916044189917' ), 0, 8), '73745418', "100 phases should produce the value we expect" );
is( substr( calculatePhases( 100, '69317163492948606335995924319873' ), 0, 8), '52432133', "100 phases should produce the value we expect" );

is( part2( '03036732577212944063491565474664' ), '84462026', "Part 2 results for this one should be 84462026" );
is( part2( '02935109699940807407585447034323' ), '78725270', "Part 2 results for this one should be 78725270" );
is( part2( '03081770884921959731165446850517' ), '53553731', "Part 2 results for this one should be 53553731" );

done_testing();
