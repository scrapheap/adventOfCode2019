use Modern::Perl;

use Day_09 qw( runIntcode );

use Test::More;
use Test::Deep;

cmp_deeply(
    runIntcode( undef, [ ], 109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99 )->{outputs},
    [ 109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99 ],
    "This intcode should return a copy of itself"
);

cmp_deeply(
    runIntcode( undef, [ ], 1102,34915192,34915192,7,4,7,99,0 ),
    superhashof( {
        outputs => array_each( re('^\d{16}$') ),
    }),
    "This intcode should return a 16 digit number"
);

cmp_deeply(
    runIntcode( undef, [ ], 104,1125899906842624,99 )->{outputs},
    [ 1125899906842624 ],
    "This intcode should return 1125899906842624"
);

done_testing();
