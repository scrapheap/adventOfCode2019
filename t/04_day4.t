use Modern::Perl;
use Test::More;

use Day_04 qw( validPasswordAttempt validPasswordAttempt2 );

ok( validPasswordAttempt(111111), "111111 should be a valid attempt");
ok( !validPasswordAttempt(223450), "223450 should not be a valid attempt - decreasing values");
ok( !validPasswordAttempt(123789), "123789 should not be a valid attempt - missing double");


ok( validPasswordAttempt2(112233), "112233 should be a valid attempt");
ok( !validPasswordAttempt2(123444), "123444 should not be a valid attempt - missing a true double");
ok( validPasswordAttempt2(111122), "111122 should be a valid attempt");

done_testing();
