use Modern::Perl;

use Day_10 qw( parseAsteroidMap countAngles destructionOrder );

use Test::More;
use Test::Deep;

my @map = (
    '.#..#',
    '.....',
    '#####',
    '....#',
    '...##',
);

cmp_deeply(
    [ parseAsteroidMap( @map ) ],
    [
        [1, 0], [4, 0],
        [0, 2], [1, 2], [2, 2], [3, 2], [4, 2],
        [4, 3],
        [3, 4], [4, 4],

    ],
    "The parsed map should produce a list of asteroid co-ordinates"
);

cmp_deeply(
    { countAngles( parseAsteroidMap( @map ) ) },
    {
        "1,0" => 7,
        "4,0" => 7,
        "0,2" => 6,
        "1,2" => 7,
        "2,2" => 7,
        "3,2" => 7,
        "4,2" => 5,
        "4,3" => 7,
        "3,4" => 8,
        "4,4" => 7,
    },
    "countAngles should return the expected hash of distinct asteroid angles"
);

my @map2 = parseAsteroidMap(
    '.#....#####...#..',
    '##...##.#####..##',
    '##...#...#.#####.',
    '..#.....#...###..',
    '..#.#.....#....##',
);

my @destructionOrder = destructionOrder( [8, 3], @map2 );

cmp_deeply( $destructionOrder[0], [8, 1], "the first asteroid destroyed should be at 8,1 ");
cmp_deeply( $destructionOrder[1], [9, 0], "the second asteroid destroyed should be at 9,0 ");
cmp_deeply( $destructionOrder[2], [9, 1], "the third asteroid destroyed should be at 9,1 ");
cmp_deeply( $destructionOrder[3], [10, 0], "the fourth asteroid destroyed should be at 10,0 ");
cmp_deeply( $destructionOrder[4], [9, 2], "the fifth asteroid destroyed should be at 9,2 ");

cmp_deeply( \@destructionOrder,
    [
        [8,  1],
        [9,  0],
        [9,  1],
        [10, 0],
        [9,  2],
        [11, 1],
        [12, 1],
        [11, 2],
        [15, 1],

        [12, 2],
        [13, 2],
        [14, 2],
        [15, 2],
        [12, 3],
        [16, 4],
        [15, 4],
        [10, 4],
        [4,  4],

        [2, 4],
        [2, 3],
        [0, 2],
        [1, 2],
        [0, 1],
        [1, 1],
        [5, 2],
        [1, 0],
        [5, 1],

        [6, 1],
        [6, 0],
        [7, 0],
        [8, 0],
        [10, 1],
        [14, 0],
        [16, 1],
        [13, 3],
        [14, 3],
    ],
    "Destruction order should be the exact order we expect"
);

my @map3 = parseAsteroidMap(
    '.#..##.###...#######',
    '##.############..##.',
    '.#.######.########.#',
    '.###.#######.####.#.',
    '#####.##.#.##.###.##',
    '..#####..#.#########',
    '####################',
    '#.####....###.#.#.##',
    '##.#################',
    '#####.##.###..####..',
    '..######..##.#######',
    '####.##.####...##..#',
    '.#####..#.######.###',
    '##...#.##########...',
    '#.##########.#######',
    '.####.#.###.###.#.##',
    '....##.##.###..#####',
    '.#.#.###########.###',
    '#.#.#.#####.####.###',
    '###.##.####.##.#..##',
);

@destructionOrder = destructionOrder( [11, 13], @map3 );

cmp_deeply( $destructionOrder[0],   [11, 12], "the 1st asteroid destroyed should be at 11,12");
cmp_deeply( $destructionOrder[1],   [12, 1],  "the 2nd asteroid destroyed should be at 12,1");
cmp_deeply( $destructionOrder[2],   [12, 2],  "the 3rd asteroid destroyed should be at 12,2");
SKIP: {
    skip "Float precision error", 1;
    cmp_deeply( $destructionOrder[9],   [12, 8],  "the 10th asteroid destroyed should be at 12,8");
}
cmp_deeply( $destructionOrder[19],  [16, 0],  "the 20th asteroid destroyed should be at 16,0");
cmp_deeply( $destructionOrder[49],  [16, 9],  "the 50th asteroid destroyed should be at 16,0");
cmp_deeply( $destructionOrder[99],  [10, 16], "the 100th asteroid destroyed should be at 10,16");
cmp_deeply( $destructionOrder[198], [9, 6],   "the 199th asteroid destroyed should be at 9,6");
cmp_deeply( $destructionOrder[199], [8, 2],   "the 200th asteroid destroyed should be at 8,2");
SKIP: {
    skip "Float precision error", 2;
    cmp_deeply( $destructionOrder[200], [10, 9],  "the 201st asteroid destroyed should be at 10,9");
    cmp_deeply( $destructionOrder[298], [11, 1],  "the 299th asteroid destroyed should be at 11,1");
}

done_testing();
