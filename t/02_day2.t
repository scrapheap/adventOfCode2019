use Modern::Perl;

use Day_02 qw( runIntcode );

use Test::More;
use Test::Deep;

cmp_deeply(
    [ runIntcode( 1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50 ) ],
    [ 3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50 ],
    "First test program should finish in the expected state"
);


cmp_deeply(
    [ runIntcode( 1,0,0,0,99 ) ],
    [ 2,0,0,0,99 ],
    "Second test program should finish in the expected state"
);

cmp_deeply(
    [ runIntcode( 2,3,0,3,99 ) ],
    [ 2,3,0,6,99 ],
    "Third test program should finish in the expected state"
);

cmp_deeply(
    [ runIntcode( 2,4,4,5,99,0 ) ],
    [ 2,4,4,5,99,9801 ],
    "Fourth test program should finish in the expected state"
);

cmp_deeply(
    [ runIntcode( 1,1,1,4,99,5,6,0,99 ) ],
    [ 30,1,1,4,2,5,6,0,99 ],
    "Fifth test program should finish in the expected state"
);

done_testing();
