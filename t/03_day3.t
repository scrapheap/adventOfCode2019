use Modern::Perl;
use Test::More;

use Day_03 qw( findNearestCrossing findNearestCrossingStepwise );

my @wires = (
    [ split /,/, "R8,U5,L5,D3" ],
    [ split /,/, "U7,R6,D4,L4" ],
);

is( findNearestCrossing( @wires ), 6, "nearest crossing should be 6 in this case" );

@wires = (
    [ split /,/, "R75,D30,R83,U83,L12,D49,R71,U7,L72" ],
    [ split /,/, "U62,R66,U55,R34,D71,R55,D58,R83" ],
);

is( findNearestCrossing( @wires ), 159, "nearest crossing should be 159 in this case" );

@wires = (
    [ split /,/, "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51" ],
    [ split /,/, "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7" ],
);

is( findNearestCrossing( @wires ), 135, "nearest crossing should be 135 in this case" );


@wires = (
    [ split /,/, "R8,U5,L5,D3" ],
    [ split /,/, "U7,R6,D4,L4" ],
);

is( findNearestCrossingStepwise( @wires ), 30, "nearest crossing should be 6 in this case" );

@wires = (
    [ split /,/, "R75,D30,R83,U83,L12,D49,R71,U7,L72" ],
    [ split /,/, "U62,R66,U55,R34,D71,R55,D58,R83" ],
);

is( findNearestCrossingStepwise( @wires ), 610, "nearest crossing should be 159 in this case" );

@wires = (
    [ split /,/, "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51" ],
    [ split /,/, "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7" ],
);

is( findNearestCrossingStepwise( @wires ), 410, "nearest crossing should be 135 in this case" );

done_testing();
