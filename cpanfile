requires 'Modern::Perl';
requires 'Test::Deep';
requires 'Algorithm::FastPermute';
requires 'Try::Tiny';
requires 'Math::Trig';
requires 'Math::Utils';
